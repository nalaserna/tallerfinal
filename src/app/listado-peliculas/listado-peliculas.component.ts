import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { PeliculasService } from '../services/peliculas.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-listado-peliculas',
  templateUrl: './listado-peliculas.component.html',
  styleUrls: ['./listado-peliculas.component.css']
})
export class ListadoPeliculasComponent implements OnInit {

  lista: any[] = [];
  
  constructor(private http: HttpClient,private servicio: PeliculasService, private router: Router) {
    servicio.getAllFilms().subscribe(resp => 
      this.lista = resp.results);
    console.log(this.lista);
   
   }
  ngOnInit() {
  }

}
