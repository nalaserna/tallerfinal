import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ListadoPeliculasComponent } from './listado-peliculas/listado-peliculas.component';
import { TriviaComponent } from './trivia/trivia.component';

const routes: Routes = [
  {path: 'listaFilms', component: ListadoPeliculasComponent},
  {path: 'trivia', component: TriviaComponent},
  {path: '', pathMatch: 'full', redirectTo: 'listaFilms'}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
