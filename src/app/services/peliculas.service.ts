import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class PeliculasService {

  constructor(private http: HttpClient) {
    
  
   }
   public getAllFilms(): Observable<any[]> {
    return this.http.get<any[]>(environment.listadoPeliculas);
  }s
}
